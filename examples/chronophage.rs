//
// chronophage.rs
// Copyright (C) 2018 Maxime “pep” Buquet <pep@bouah.net>
// Distributed under terms of the LGPLv3+ license.
//

#![feature(exclusive_range_pattern)]

use clap::{
    app_from_crate,
    clap_app,
    crate_authors,
    crate_description,
    crate_name,
    crate_version,
};
use try_from::TryFrom;

use chrono::{Date, NaiveDate, TimeZone, Utc};
use timelog::errors::Error;
use timelog::types::{
    ChronophageDay, ChronophageDays, Task, TimelogDay, TimelogEntry, TimesheetDay,
};

use reqwest::header::USER_AGENT;

fn push_timesheets<D: Into<ChronophageDays>>(
    user: &str,
    password: &str,
    days: D,
) -> Result<(), String> {
    let days = days.into();
    let server_url = "https://dev.chronophage.collabora.com";
    let api_endpoint = "/timesheet";

    let client = reqwest::Client::new();
    let res = client
        .post(&format!("{}{}", server_url, api_endpoint))
        .header(USER_AGENT, "timelog-rs")
        .basic_auth(user, Some(password))
        .form(&days)
        .send();

    if let Ok(mut res) = res {
        match res.status().as_u16() {
            100..199 | 200..299 | 300..399 => return Ok(()),
            401 => return Err(String::from("Wrong credentials.")),
            400 | 402..499 => {
                if let Ok(text) = res.text() {
                    return Err(text);
                }
            }
            500..599 => return Err(String::from("Server error, please retry later.")),
            n => return Err(format!("Unknown status code: {}", n)),
        }
    }

    Err(String::new())
}

fn chronophage_days() -> Result<Vec<ChronophageDay>, Error> {
    let task1 = Task::new(vec!["foo", "bar", "hah", "hoh"]);
    let task2 = Task::new(vec!["baz", "qxx", "hqh", "hxh"]);

    let timelogs = vec![
        TimelogDay {
            date: Date::from_utc(NaiveDate::from_ymd(2018, 7, 3), Utc),
            entries: vec![
                TimelogEntry::new_slack(Utc.ymd(2018, 7, 3).and_hms(10, 0, 0), Some("Arrived **")),
                TimelogEntry::new_full(
                    Utc.ymd(2018, 7, 3).and_hms(13, 0, 0),
                    task1.clone(),
                    Some("Test"),
                ),
                TimelogEntry::new_full(
                    Utc.ymd(2018, 7, 3).and_hms(18, 0, 0),
                    task1.clone(),
                    Some("Test"),
                ),
            ],
        },
        TimelogDay {
            date: Date::from_utc(NaiveDate::from_ymd(2018, 7, 4), Utc),
            entries: vec![
                TimelogEntry::new_slack(Utc.ymd(2018, 7, 4).and_hms(10, 5, 0), Some("Arrived **")),
                TimelogEntry::new_full(
                    Utc.ymd(2018, 7, 4).and_hms(13, 0, 0),
                    task1.clone(),
                    Some("Test"),
                ),
                TimelogEntry::new_full(
                    Utc.ymd(2018, 7, 4).and_hms(18, 0, 0),
                    task2.clone(),
                    Some("Test"),
                ),
            ],
        },
        TimelogDay {
            date: Date::from_utc(NaiveDate::from_ymd(2018, 7, 5), Utc),
            entries: vec![
                TimelogEntry::new_slack(Utc.ymd(2018, 7, 5).and_hms(18, 5, 0), Some("Arrived **")),
                TimelogEntry::new_full(
                    Utc.ymd(2018, 7, 5).and_hms(19, 0, 0),
                    task2.clone(),
                    Some("Test"),
                ),
                TimelogEntry::new_slack(Utc.ymd(2018, 7, 5).and_hms(20, 0, 0), Some("Foo **")),
                TimelogEntry::new_full(
                    Utc.ymd(2018, 7, 6).and_hms(2, 0, 0),
                    task2.clone(),
                    Some("Test"),
                ),
            ],
        },
    ];

    let timesheets: Vec<TimesheetDay> = timelogs.into_iter().map(TimesheetDay::from).collect();

    return timesheets.into_iter().try_fold(
        vec![],
        |mut acc, d| -> Result<Vec<ChronophageDay>, Error> {
            let d = ChronophageDay::try_from(d)?;
            acc.push(d);
            Ok(acc)
        },
    );
}

fn main() {
    let _matches = clap_app!(@app (app_from_crate!())
        (@arg config: -c --config +takes_value "Config file path")
        (@arg user: -u --user +takes_value +required "HTTP Basic Auth user")
        (@arg password: -p --password +takes_value +required "HTTP Basic Auth password")
    )
    .get_matches();

    let user = "Foo";
    let password = "Bar";

    if let Ok(days) = chronophage_days() {
        if let Err(text) = push_timesheets(user, password, days) {
            println!("An error occured: {}", text);
        } else {
            println!("Timelogs successfully uploaded.");
        }
    }
}
