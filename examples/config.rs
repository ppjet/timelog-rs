//
// toml.rs
// Copyright (C) 2018 Maxime “pep” Buquet <pep@bouah.net>
// Distributed under terms of the LGPLv3+ license.
//

use clap::{
    ArgMatches,
    app_from_crate,
    clap_app,
    crate_authors,
    crate_description,
    crate_name,
    crate_version,
};

use directories::ProjectDirs;
use serde_derive::Deserialize;
use url::Url;

use std::fs::File;
use std::io::prelude::*;
use std::path::{Path, PathBuf};

#[derive(Debug)]
pub enum TimelogError {
    MissingConfiguration,
}

#[derive(Debug)]
pub enum Error {
    Timelog(TimelogError),
    IO(std::io::Error),
    Toml(toml::de::Error),
}

impl From<TimelogError> for Error {
    fn from(err: TimelogError) -> Error {
        Error::Timelog(err)
    }
}

impl From<std::io::Error> for Error {
    fn from(err: std::io::Error) -> Error {
        Error::IO(err)
    }
}

impl From<toml::de::Error> for Error {
    fn from(err: toml::de::Error) -> Error {
        Error::Toml(err)
    }
}

#[derive(Debug, Clone, Deserialize)]
pub struct Config {
    http_user: String,
    http_password: String,
    #[serde(with = "url_serde")]
    report_to_url: Url,
    #[serde(with = "url_serde")]
    task_list_url: Url,
}

pub fn read_file<P: AsRef<Path>>(path: P) -> Result<String, Error> {
    let mut fh = File::open(path)?;
    let mut file = String::new();

    fh.read_to_string(&mut file)?;
    Ok(file)
}

pub fn get_config(matches: ArgMatches) -> Result<Config, Error> {
    let pdirs = ProjectDirs::from("com.collabora", "Collabora", "timelog-rs");

    let config_path = match matches.value_of("config") {
        Some(path) => PathBuf::from(path),
        None => match pdirs {
            Some(dirs) => dirs.config_dir().join("config.toml"),
            None => return Err(Error::Timelog(TimelogError::MissingConfiguration)),
        },
    };

    let config_str = read_file(config_path)?;
    let mut config: Config = toml::from_str(config_str.as_str())?;

    match matches.value_of("http_user") {
        Some(user) => config.http_user = String::from(user),
        _ => (),
    }

    match matches.value_of("http_password") {
        Some(passwd) => config.http_password = String::from(passwd),
        _ => (),
    }

    Ok(config)
}

fn main() {
    let matches = clap_app!(@app (app_from_crate!())
        (@arg config: -c --config +takes_value "Config file path")
        (@arg http_user: -u --http_user +takes_value +required "HTTP Basic Auth user")
        (@arg http_password: -p --http_password +takes_value +required "HTTP Basic Auth password")
    )
    .get_matches();

    println!("Foo: {:?}", get_config(matches));
}
