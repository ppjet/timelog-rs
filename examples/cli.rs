//
// cli.rs
// Copyright (C) 2018 Maxime “pep” Buquet <pep@bouah.net>
// Distributed under terms of the LGPLv3+ license.
//

use clap::{
    app_from_crate,
    clap_app,
    crate_authors,
    crate_description,
    crate_name,
    crate_version,
};

use std::fs::File;
use std::io::prelude::*;
use std::io::Error as IOError;
use std::path::Path;

pub fn read_file<P: AsRef<Path>>(path: P) -> Result<String, IOError> {
    let mut fh = File::open(path)?;
    let mut file = String::new();

    fh.read_to_string(&mut file)?;
    Ok(file)
}

fn main() {
    env_logger::init();

    let matches = clap_app!(@app (app_from_crate!())
        (@arg file: -f --file +takes_value +required "File to parse.")
    )
    .get_matches();

    let filename = matches.value_of("file").unwrap();
    let timelogs = read_file(filename).unwrap();
    println!("Timelogs: {:?}", timelogs);
}
