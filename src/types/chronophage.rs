//
// chronophage.rs
// Copyright (C) 2018 Maxime “pep” Buquet <pep@bouah.net>
// Distributed under terms of the LGPLv3+ license.
//

use crate::errors::ChronophageError as Error;
use chrono::{Date, Duration, Utc};
use serde::{ser::SerializeMap, Serialize, Serializer};

use crate::types::{EntryType, Task as BaseTask, TimesheetDay, TimesheetItem};
use std::fmt;
use try_from::TryFrom;

/// Tasks in Collabora Chronophage are more restricted than standard gtimelog
/// tasks. They must contain 4 identifiers, and these identifiers have specific
/// roles in Chronophage itself.
#[derive(Debug, Clone, PartialEq)]
pub struct Task {
    pub category: String,
    pub project: String,
    pub activity: String,
    pub task: String,
}

impl Task {
    pub fn new<S: Into<String>>(category: S, project: S, activity: S, task: S) -> Task {
        Task {
            category: category.into(),
            project: project.into(),
            activity: activity.into(),
            task: task.into(),
        }
    }
}

impl TryFrom<BaseTask> for Task {
    type Err = Error;

    fn try_from(task: BaseTask) -> Result<Task, Error> {
        let mut ids = task.identifiers;

        if ids.len() != 4 {
            return Err(Error::IncompatibleBaseTask);
        }

        Ok(Task {
            task: ids.pop().unwrap_or_else(|| String::new()),
            activity: ids.pop().unwrap_or_else(|| String::new()),
            project: ids.pop().unwrap_or_else(|| String::new()),
            category: ids.pop().unwrap_or_else(|| String::new()),
        })
    }
}

impl Into<BaseTask> for Task {
    fn into(self) -> BaseTask {
        BaseTask::new(vec![self.category, self.project, self.activity, self.task])
    }
}

impl<'a> From<&'a Task> for String {
    fn from(t: &Task) -> String {
        format!(
            "{}: {}: {}: {}",
            &t.category, &t.project, &t.activity, &t.task
        )
    }
}

impl From<Task> for String {
    fn from(t: Task) -> String {
        String::from(&t)
    }
}

impl fmt::Display for Task {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", String::from(self))
    }
}

impl Serialize for Task {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        serializer.serialize_str(&String::from(self))
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct Entry {
    pub duration: Duration,
    pub task: Task,
    pub comment: Option<String>,
}

impl Entry {
    fn new<S: Into<String>>(duration: Duration, task: Task, comment: Option<S>) -> Entry {
        Entry {
            duration,
            task,
            comment: comment.map(Into::into),
        }
    }
}

pub fn duration_to_string(d: &Duration) -> String {
    let hours = d.num_minutes() / 60;
    let minutes = d.num_minutes() - hours * 60;
    format!("{}:{:0>2}", hours, minutes)
}

impl<'a> From<&'a Entry> for String {
    fn from(e: &Entry) -> String {
        let d = duration_to_string(&e.duration);
        match &e.comment {
            None => format!("{}: {}:", d, &e.task),
            Some(ref c) => format!("{} {}: {}", d, &e.task, c),
        }
    }
}

impl From<Entry> for String {
    fn from(entry: Entry) -> String {
        String::from(&entry)
    }
}

impl fmt::Display for Entry {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", String::from(self))
    }
}

impl Serialize for Entry {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        serializer.serialize_str(&String::from(self))
    }
}

impl TryFrom<TimesheetItem> for Entry {
    type Err = Error;

    fn try_from(timesheet: TimesheetItem) -> Result<Entry, Error> {
        match timesheet.entry {
            EntryType::Slack => {
                return Err(Error::IncompatibleBaseTask);
            }
            EntryType::Full(task) => Ok(Entry::new(
                timesheet.duration,
                Task::try_from(task)?,
                timesheet.comment,
            )),
        }
    }
}

/// Entries sent to Chronophage do not contain slack type entries, these are
/// only kept locally.
#[derive(Debug, Clone, PartialEq)]
pub struct Day {
    date: Date<Utc>,
    items: Vec<Entry>,
}

impl Day {
    pub fn new(date: Date<Utc>, items: Vec<Entry>) -> Day {
        Day { date, items }
    }
}

// TODO: This is a really poor attempt at being able to Serialize Day and
// Vec<Day> the same way. There must be a better way. Halp.
#[derive(Debug, Clone, PartialEq)]
pub struct Days {
    pub days: Vec<Day>,
}

impl Days {
    pub fn new(days: Vec<Day>) -> Days {
        Days { days }
    }
}

impl From<Day> for Days {
    fn from(day: Day) -> Days {
        Days { days: vec![day] }
    }
}

impl From<Vec<Day>> for Days {
    fn from(vec: Vec<Day>) -> Days {
        Days { days: vec }
    }
}

impl Into<Vec<Day>> for Days {
    fn into(self: Days) -> Vec<Day> {
        self.days
    }
}

impl Serialize for Days {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let mut map = serializer.serialize_map(Some(self.days.len()))?;
        self.days
            .iter()
            .try_for_each(|day| -> Result<(), S::Error> {
                let s_date = day.date.format("%Y-%m-%d").to_string();
                let s_items = day.items.iter().fold(String::new(), |mut s, item| {
                    s.push_str(String::from(item).as_str());
                    s.push_str("\n");
                    s
                });
                map.serialize_entry(&s_date, &s_items)?;
                Ok(())
            })?;
        map.end()
    }
}

impl TryFrom<TimesheetDay> for Day {
    type Err = Error;

    fn try_from(day: TimesheetDay) -> Result<Day, Error> {
        let items = day.items.into_iter().try_fold(
            vec![],
            |mut acc, tentry| -> Result<Vec<Entry>, Error> {
                match tentry {
                    TimesheetItem {
                        entry: EntryType::Slack,
                        ..
                    } => (),
                    item @ TimesheetItem {
                        entry: EntryType::Full(_),
                        ..
                    } => {
                        acc.push(Entry::try_from(item)?);
                    }
                }

                return Ok(acc);
            },
        )?;

        Ok(Day {
            date: day.date,
            items,
        })
    }
}

#[cfg(test)]
mod test {
    use super::{Day, Days, Entry, Task};
    use crate::errors::ChronophageError as Error;
    use crate::types::{Task as BaseTask, TimesheetDay, TimesheetItem};
    use chrono::{Date, Duration, NaiveDate, Utc};
    use serde_urlencoded;

    use try_from::TryFrom;

    #[test]
    fn display_task() {
        let task = Task::new("foo", "bar", "baz", "qxx");
        assert_eq!(format!("{}", task), String::from("foo: bar: baz: qxx"));
    }

    #[test]
    fn new_task() {
        let task = Task::new("foo", "bar", "baz", "qxx");

        let result = Task {
            category: String::from("foo"),
            project: String::from("bar"),
            activity: String::from("baz"),
            task: String::from("qxx"),
        };

        assert_eq!(task, result);
    }

    #[test]
    fn from_task() {
        let task = BaseTask::new(vec!["foo", "bar", "baz", "qxx"]);
        let result = Task::new("foo", "bar", "baz", "qxx");

        match Task::try_from(task) {
            Ok(task) => assert_eq!(task, result),
            Err(_) => panic!(),
        }
    }

    #[test]
    fn from_task_err() {
        let task = BaseTask::new(vec!["foo", "bar", "baz"]);

        match Task::try_from(task) {
            Err(Error::IncompatibleBaseTask) => (),
            Ok(_) => panic!(),
        }
    }

    #[test]
    fn display_entry() {
        let entry = Entry::new(
            Duration::hours(1) + Duration::minutes(5),
            Task::new("foo", "bar", "baz", "qxx"),
            Some("hah"),
        );
        assert_eq!(
            format!("{}", entry),
            String::from("1:05 foo: bar: baz: qxx: hah")
        );
    }

    #[test]
    fn new_chronophage_entry() {
        let entry = Entry::new(
            Duration::minutes(5),
            Task::new("foo", "bar", "baz", "qxx"),
            Some("hah"),
        );

        let result = Entry {
            duration: Duration::minutes(5),
            task: Task::new("foo", "bar", "baz", "qxx"),
            comment: Some(String::from("hah")),
        };

        assert_eq!(entry, result);
    }

    #[test]
    fn serialize_day() {
        let days = Days::from(Day::new(
            Date::from_utc(NaiveDate::from_ymd(2018, 7, 5), Utc),
            vec![
                Entry::new(
                    Duration::minutes(5),
                    Task::new("foo", "bar", "baz", "qxx"),
                    Some("Test"),
                ),
                Entry::new(
                    Duration::hours(7),
                    Task::new("foo", "bar", "baz", "qxx"),
                    Some("Test"),
                ),
            ],
        ));

        let result = "2018-07-05=0%3A05+foo%3A+bar%3A+baz%3A+qxx%3A+Test%0A7%3A00+foo%3A+bar%3A+baz%3A+qxx%3A+Test%0A";

        assert_eq!(serde_urlencoded::to_string(days), Ok(String::from(result)));
    }

    #[test]
    fn serialize_days() {
        let days = Days::new(vec![
            Day::new(
                Date::from_utc(NaiveDate::from_ymd(2018, 7, 5), Utc),
                vec![
                    Entry::new(
                        Duration::minutes(5),
                        Task::new("foo", "bar", "baz", "qxx"),
                        Some("Test"),
                    ),
                    Entry::new(
                        Duration::hours(7),
                        Task::new("foo", "bar", "baz", "qxx"),
                        None::<String>,
                    ),
                ],
            ),
            Day::new(
                Date::from_utc(NaiveDate::from_ymd(2018, 7, 6), Utc),
                vec![
                    Entry::new(
                        Duration::hours(2) + Duration::minutes(53),
                        Task::new("foo", "bar", "baz", "qxx"),
                        None::<String>,
                    ),
                    Entry::new(
                        Duration::hours(7),
                        Task::new("foo", "bar", "baz", "qxx"),
                        Some("Test"),
                    ),
                ],
            ),
        ]);

        let result = "2018-07-05=0%3A05+foo%3A+bar%3A+baz%3A+qxx%3A+Test%0A7%3A00%3A+foo%3A+bar%3A+baz%3A+qxx%3A%0A\
        &2018-07-06=2%3A53%3A+foo%3A+bar%3A+baz%3A+qxx%3A%0A7%3A00+foo%3A+bar%3A+baz%3A+qxx%3A+Test%0A";

        assert_eq!(serde_urlencoded::to_string(days), Ok(String::from(result)));
    }

    #[test]
    fn from_timelog_day() {
        let task = BaseTask::new(vec!["foo", "bar", "baz", "qxx"]);

        let timesheet_day = TimesheetDay {
            date: Date::from_utc(NaiveDate::from_ymd(2018, 7, 5), Utc),
            items: vec![
                TimesheetItem::new_full(Duration::minutes(55), task.clone(), Some("Test")),
                TimesheetItem::new_full(Duration::hours(7), task.clone(), Some("Test")),
            ],
        };

        let result = Day {
            date: Date::from_utc(NaiveDate::from_ymd(2018, 7, 5), Utc),
            items: vec![
                Entry::new(
                    Duration::minutes(55),
                    Task::new("foo", "bar", "baz", "qxx"),
                    Some("Test"),
                ),
                Entry::new(
                    Duration::hours(7),
                    Task::new("foo", "bar", "baz", "qxx"),
                    Some("Test"),
                ),
            ],
        };

        match Day::try_from(timesheet_day) {
            Ok(day) => assert_eq!(day, result),
            Err(_) => panic!(),
        }
    }

    #[test]
    fn from_timelog_day_remove_slack() {
        let task = BaseTask::new(vec!["foo", "bar", "baz", "qxx"]);

        let timesheet_day = TimesheetDay {
            date: Date::from_utc(NaiveDate::from_ymd(2018, 7, 5), Utc),
            items: vec![
                TimesheetItem::new_full(Duration::minutes(55), task.clone(), Some("Test")),
                TimesheetItem::new_slack(Duration::hours(1), Some("Slack!")),
                TimesheetItem::new_full(Duration::hours(7), task.clone(), Some("Test")),
            ],
        };

        let result = Day {
            date: Date::from_utc(NaiveDate::from_ymd(2018, 7, 5), Utc),
            items: vec![
                Entry::new(
                    Duration::minutes(55),
                    Task::new("foo", "bar", "baz", "qxx"),
                    Some("Test"),
                ),
                Entry::new(
                    Duration::hours(7),
                    Task::new("foo", "bar", "baz", "qxx"),
                    Some("Test"),
                ),
            ],
        };

        match Day::try_from(timesheet_day) {
            Ok(day) => assert_eq!(day, result),
            Err(_) => panic!(),
        }
    }
}
