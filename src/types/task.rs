//
// types/task.rs
// Copyright (C) 2018 Maxime “pep” Buquet <pep@bouah.net>
// Distributed under terms of the LGPLv3+ license.
//

/// This structure is the base brick for most types in this library. It defines
/// a task, that is, an ordered list of identifiers. A minimum number of
/// identifiers per task is not enforced.
#[derive(Debug, Clone, PartialEq)]
pub struct Task {
    pub identifiers: Vec<String>,
}

impl Task {
    pub fn new<S: Into<String>>(identifiers: Vec<S>) -> Task {
        Task {
            identifiers: identifiers.into_iter().map(Into::into).collect(),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn new() {
        let task = Task::new(vec!["foo", "bar", "baz", "qxx"]);
        let result = Task {
            identifiers: vec![
                String::from("foo"),
                String::from("bar"),
                String::from("baz"),
                String::from("qxx"),
            ],
        };

        assert_eq!(task, result);
    }
}
