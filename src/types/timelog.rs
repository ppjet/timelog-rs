//
// timelog_day.rs
// Copyright (C) 2018 Maxime “pep” Buquet <pep@bouah.net>
// Distributed under terms of the LGPLv3+ license.
//

use crate::types::Task;
use chrono::{Date, DateTime, Utc};
use std::cmp::Ordering;

/// There are multiple types of entries to use in timelogs. This defines a
/// standard (`Full`) entry, and a slack (`Slack`) entry.
///
/// A `Full` entry will influence the total amount of hours spent during the
/// day, whereas the period of time attributed to a `Slack` entry will not
/// count towards that total amount.
#[derive(Debug, Clone, PartialEq)]
pub enum EntryType {
    Full(Task),
    Slack,
}

/// This structure provides a way to define a single activity that finished at
/// a specific date. It is only relevant within a
/// [`TimelogDay`](struct.TimelogDay.html).
/// The comment provides an extra description for the activity.
#[derive(Debug, Clone, PartialEq)]
pub struct TimelogEntry {
    pub datetime: DateTime<Utc>,
    pub entry: EntryType,
    pub comment: Option<String>,
}

impl PartialOrd for TimelogEntry {
    fn partial_cmp(&self, other: &TimelogEntry) -> Option<Ordering> {
        self.datetime.partial_cmp(&other.datetime)
    }
}

impl TimelogEntry {
    pub fn new<S: Into<String>>(
        datetime: DateTime<Utc>,
        entry: EntryType,
        comment: Option<S>,
    ) -> TimelogEntry {
        TimelogEntry {
            datetime,
            entry,
            comment: comment.map(Into::into),
        }
    }

    pub fn new_full<S: Into<String>>(
        datetime: DateTime<Utc>,
        task: Task,
        comment: Option<S>,
    ) -> TimelogEntry {
        TimelogEntry::new(datetime, EntryType::Full(task), comment)
    }

    pub fn new_slack<S: Into<String>>(datetime: DateTime<Utc>, comment: Option<S>) -> TimelogEntry {
        TimelogEntry::new(datetime, EntryType::Slack, comment)
    }

    pub fn is_full(&self) -> bool {
        match &self.entry {
            EntryType::Full(_) => true,
            EntryType::Slack => false,
        }
    }

    pub fn is_slack(&self) -> bool {
        match &self.entry {
            EntryType::Slack => true,
            EntryType::Full(_) => false,
        }
    }
}

/// This represents a timelogged day.
/// [`TimelogEntry`](struct.TimelogEntry.html)s in this day will all count
/// towards the same total, even if dates of these entries are not of the same
/// day as the [`TimelogDay`](struct.timelogDay.html) itself.
///
/// ```rust
/// # extern crate chrono;
/// # extern crate timelog;
/// #
/// # use timelog::types::{Task, TimelogEntry, TimelogDay};
/// # use chrono::{Date, NaiveDate, TimeZone, Utc};
/// #
/// # fn main() {
/// let task1 = Task::new(vec![
///     "foo",
///     "bar",
/// ]);
/// let task2 = Task::new(vec![
///     "baz",
///     "qxx",
/// ]);
/// let day = TimelogDay {
///     date: Date::from_utc(NaiveDate::from_ymd(2018, 7, 3), Utc),
///     entries: vec![
///         TimelogEntry::new_slack(Utc.ymd(2018, 7, 3).and_hms(10, 0, 0), Some("slack1 **")),
///         TimelogEntry::new_full(Utc.ymd(2018, 7, 3).and_hms(15, 0, 0), task1, Some("comment2")),
///         TimelogEntry::new_full(Utc.ymd(2018, 7, 4).and_hms(2, 0, 0), task2, Some("comment3")),
///     ],
/// };
/// # println!("{:?}", day);
/// # }
/// ```
#[derive(Debug, PartialEq)]
pub struct TimelogDay {
    pub date: Date<Utc>,
    pub entries: Vec<TimelogEntry>,
}

#[cfg(test)]
mod test {
    use crate::types::{EntryType, Task, TimelogEntry};
    use chrono::{TimeZone, Utc};

    #[test]
    fn new_slack() {
        let entry = TimelogEntry::new_slack(Utc.ymd(2018, 7, 3).and_hms(10, 0, 0), Some("Comment"));

        let result = TimelogEntry {
            datetime: Utc.ymd(2018, 7, 3).and_hms(10, 0, 0),
            entry: EntryType::Slack,
            comment: Some(String::from("Comment")),
        };

        assert_eq!(entry, result);
    }

    #[test]
    fn new_no_comment() {
        let entry = TimelogEntry::new_slack(Utc.ymd(2018, 7, 3).and_hms(10, 0, 0), None::<String>);

        let result = TimelogEntry {
            datetime: Utc.ymd(2018, 7, 3).and_hms(10, 0, 0),
            entry: EntryType::Slack,
            comment: None,
        };

        assert_eq!(entry, result);
    }

    #[test]
    fn new_full() {
        let entry = TimelogEntry::new_full(
            Utc.ymd(2018, 7, 3).and_hms(10, 0, 0),
            Task::new(vec!["foo", "bar"]),
            Some("comment"),
        );

        let result = TimelogEntry {
            datetime: Utc.ymd(2018, 7, 3).and_hms(10, 0, 0),
            entry: EntryType::Full(Task::new(vec!["foo", "bar"])),
            comment: Some(String::from("comment")),
        };

        assert_eq!(entry, result);
    }
}
