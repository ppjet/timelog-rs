//
// types/timesheet_item.rs
// Copyright (C) 2018 Maxime “pep” Buquet <pep@bouah.net>
// Distributed under terms of the LGPLv3+ license.
//

use crate::types::{EntryType, Task, TimelogDay, TimelogEntry};
use chrono::{Date, Duration, Utc};

///
#[derive(Debug, PartialEq)]
pub struct TimesheetItem {
    pub duration: Duration,
    pub entry: EntryType,
    pub comment: Option<String>,
}

impl TimesheetItem {
    pub fn new<S: Into<String>>(
        duration: Duration,
        entry: EntryType,
        comment: Option<S>,
    ) -> TimesheetItem {
        TimesheetItem {
            duration,
            entry,
            comment: comment.map(Into::into),
        }
    }

    pub fn new_full<S: Into<String>>(
        duration: Duration,
        task: Task,
        comment: Option<S>,
    ) -> TimesheetItem {
        TimesheetItem::new(duration, EntryType::Full(task), comment.map(Into::into))
    }

    pub fn new_slack<S: Into<String>>(duration: Duration, comment: Option<S>) -> TimesheetItem {
        TimesheetItem::new(duration, EntryType::Slack, comment.map(Into::into))
    }

    pub fn from_timelog(tentry: &TimelogEntry, last_tentry: &TimelogEntry) -> TimesheetItem {
        TimesheetItem::new(
            tentry.datetime - last_tentry.datetime,
            tentry.entry.clone(),
            tentry.comment.clone(),
        )
    }
}

#[derive(Debug, PartialEq)]
pub struct TimesheetDay {
    pub date: Date<Utc>,
    pub items: Vec<TimesheetItem>,
}

impl From<TimelogDay> for TimesheetDay {
    fn from(day: TimelogDay) -> TimesheetDay {
        let items = day
            .entries
            .into_iter()
            .fold((vec![], None), |(mut acc, last_tentry), tentry| {
                // TODO: Sort entries, or fail if not sorted

                match last_tentry {
                    None => (),
                    Some(last) => acc.push(TimesheetItem::from_timelog(&tentry, &last)),
                }

                return (acc, Some(tentry));
            })
            .0;

        TimesheetDay {
            date: day.date,
            items,
        }
    }
}

#[cfg(test)]
mod test {
    use crate::types::{Task, TimelogDay, TimelogEntry, TimesheetDay, TimesheetItem};
    use chrono::{Date, Duration, NaiveDate, TimeZone, Utc};

    #[test]
    fn test_timesheet_from_day_empty() {
        let day = TimelogDay {
            date: Date::from_utc(NaiveDate::from_ymd(2018, 7, 3), Utc),
            entries: vec![],
        };

        let result = TimesheetDay {
            date: Date::from_utc(NaiveDate::from_ymd(2018, 7, 3), Utc),
            items: vec![],
        };

        assert_eq!(TimesheetDay::from(day), result);
    }

    #[test]
    fn test_timesheet_from_day_slack_entries() {
        let day = TimelogDay {
            date: Date::from_utc(NaiveDate::from_ymd(2018, 7, 3), Utc),
            entries: vec![
                TimelogEntry::new_slack(Utc.ymd(2018, 7, 3).and_hms(10, 0, 0), Some("slack1 **")),
                TimelogEntry::new_slack(Utc.ymd(2018, 7, 3).and_hms(13, 0, 0), Some("slack2 **")),
                TimelogEntry::new_slack(Utc.ymd(2018, 7, 3).and_hms(15, 0, 0), Some("slack3 **")),
            ],
        };

        let result = TimesheetDay {
            date: Date::from_utc(NaiveDate::from_ymd(2018, 7, 3), Utc),
            items: vec![
                TimesheetItem::new_slack(Duration::hours(3), Some("slack2 **")),
                TimesheetItem::new_slack(Duration::hours(2), Some("slack3 **")),
            ],
        };

        assert_eq!(TimesheetDay::from(day), result);
    }

    #[test]
    fn test_timesheet_from_day() {
        let day = TimelogDay {
            date: Date::from_utc(NaiveDate::from_ymd(2018, 7, 3), Utc),
            entries: vec![
                TimelogEntry::new_slack(Utc.ymd(2018, 7, 3).and_hms(10, 0, 0), Some("slack1 **")),
                TimelogEntry::new_full(
                    Utc.ymd(2018, 7, 3).and_hms(12, 15, 0),
                    Task::new(vec!["project1", "category1", "activity1", "task1"]),
                    Some("comment1"),
                ),
                TimelogEntry::new_slack(Utc.ymd(2018, 7, 3).and_hms(13, 0, 0), Some("lunch")),
                TimelogEntry::new_full(
                    Utc.ymd(2018, 7, 3).and_hms(18, 0, 0),
                    Task::new(vec!["project2", "category2", "activity2", "task2"]),
                    Some("comment2"),
                ),
            ],
        };

        let result = TimesheetDay {
            date: Date::from_utc(NaiveDate::from_ymd(2018, 7, 3), Utc),
            items: vec![
                TimesheetItem::new_full(
                    Duration::hours(2) + Duration::minutes(15),
                    Task::new(vec!["project1", "category1", "activity1", "task1"]),
                    Some("comment1"),
                ),
                TimesheetItem::new_slack(Duration::minutes(45), Some("lunch")),
                TimesheetItem::new_full(
                    Duration::hours(5),
                    Task::new(vec!["project2", "category2", "activity2", "task2"]),
                    Some("comment2"),
                ),
            ],
        };

        assert_eq!(TimesheetDay::from(day), result);
    }
}
