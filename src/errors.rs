//
// errors.rs
// Copyright (C) 2018 Maxime “pep” Buquet <pep@bouah.net>
// Distributed under terms of the LGPLv3+ license.
//

#[derive(Debug)]
pub enum ChronophageError {
    IncompatibleBaseTask,
}

#[derive(Debug)]
pub enum Error {
    ChronophageError(ChronophageError),
}

impl From<ChronophageError> for Error {
    fn from(err: ChronophageError) -> Error {
        Error::ChronophageError(err)
    }
}
